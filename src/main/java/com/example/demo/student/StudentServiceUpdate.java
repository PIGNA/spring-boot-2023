package com.example.demo.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

@Service
public class StudentServiceUpdate {
    public final StudentRepository studentRepository;

    @Autowired
    public StudentServiceUpdate(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    @Transactional
    public void updateStudent(Long studentId, String newLastName,String newFirstName, String newEmail) {

        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new IllegalStateException(
                        "student with id " + studentId + " does not exist"));

        if (newLastName != null &&
                newLastName.length() > 0 &&
                !Objects.equals(student.getLastName(), newLastName)) {
            student.setLastName(newLastName);
        }
        if (newFirstName != null &&
                newFirstName.length() > 0 &&
                !Objects.equals(student.getFirstName(), newFirstName)) {
            student.setFirstName(newFirstName);
        }
        if (newEmail != null &&
                newEmail.length() > 0 &&
                !Objects.equals(student.getEmail(), newEmail)) {
            Optional<Student> studentCheckNewEmail = studentRepository
                    .findStudentByEmail(newEmail);
            if(studentCheckNewEmail.isPresent()){
                throw new IllegalStateException("Email taken");
            }
            student.setEmail(newEmail);
        }
    }
}
