package com.example.demo.student;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/v1/student")
public class StudentControllerUpdate {
    private final StudentServiceUpdate studentServiceUpdate;

    public StudentControllerUpdate(StudentServiceUpdate studentServiceUpdate) {
        this.studentServiceUpdate = studentServiceUpdate;
    }
    @PutMapping(path= "{studentId}")
    public void updateStudent(@PathVariable("studentId") Long studentId,
                              @RequestParam(required = false) String lastName,
                              @RequestParam(required = false) String firstName,
                              @RequestParam(required = false) String email){
        studentServiceUpdate.updateStudent(studentId, lastName,firstName, email);

    }
}
