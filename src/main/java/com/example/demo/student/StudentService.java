package com.example.demo.student;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public record StudentService (StudentRepository studentRepository){

    /*
     * Read all student
     * */
    public List<Student> getStudent(){
        return  studentRepository.findAll();
    }

    /*
    * Read student by studentId
    * */
    public Optional<Student> getStudentById(Long studentId){
        return studentRepository.findById(studentId);
    }

    /*
     * Add new student
     * */
    public void addNewStudent(Student student) {
        Optional<Student> studentOptional = studentRepository.findStudentByEmail(student.getEmail());
        if (studentOptional.isPresent()){
            throw new IllegalStateException("Email taken");
        }
        studentRepository.save(student);   }

    /*
     * Delete one student
     * */
    public void deleStudent(Long studentId) {
        boolean exist = studentRepository.existsById(studentId);
        if(!exist){
            throw new IllegalStateException("student with id " + studentId + " does not exist");
        }
        studentRepository.deleteById(studentId);
    }
}
