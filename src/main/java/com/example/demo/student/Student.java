package com.example.demo.student;

import java.time.LocalDate;
import java.time.Period;

import jakarta.persistence.*;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Entity(name = "Student")
@Table(uniqueConstraints =
    @UniqueConstraint(name = "student_email_unique", columnNames = "email"))
public class Student {
    @Id
    @SequenceGenerator(
            name = "student_sequence",
            sequenceName = "student_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "student_sequence"
    )
    @Column(updatable = false)
    private Long id;

    @NonNull
    @Column(columnDefinition = "TEXT")
    private String lastName;

    @NonNull
    @Column(columnDefinition = "TEXT")
    private String firstName;

    @NonNull
    @Column(
            name = "email",
            columnDefinition = "TEXT",
            updatable = true)
    private String email;

    @NonNull
    @Column(columnDefinition = "date")
    private LocalDate dob;
    @Transient
    private Integer age;

    public  Integer getAge(){
        return Period.between(this.dob,LocalDate.now()).getYears();
    }

}