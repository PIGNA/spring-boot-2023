package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

/*
* This was just for testing
* */
@Configuration
public class StudentConfig {
    @Bean
   CommandLineRunner commandLineRunner(StudentRepository studentRepository){
       return  args -> {
           studentRepository.saveAll(List.of( new Student(
                   "mendosa",
                   "teresa",
                   "mendosa@gmail.com",
                   LocalDate.of(2000,Month.AUGUST,5)
           ),new Student(
                   "gonzales",
                   "antonio",
                   "gonzales@gmail.com",
                   LocalDate.of(2000,Month.AUGUST,5)
           )));
       };
   }
}
