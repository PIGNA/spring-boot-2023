package com.example.demo.student;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/v1/student")
public record StudentController (StudentService studentService){


    @GetMapping
    public List<Student> getStudent(){
        return studentService.getStudent();
    }

    @GetMapping(path= "{studentId}")
    public Optional<Student> getStudentById(@PathVariable("studentId") Long studentId){
        return  studentService.getStudentById(studentId);
    }
    @PostMapping
    public void registerNewStudent(@RequestBody Student student){
        studentService.addNewStudent(student);
    }

    @DeleteMapping(path= "{studentId}")
    public void deleteStudent(@PathVariable("studentId") Long studentId){
        studentService.deleStudent(studentId);
    }

}
